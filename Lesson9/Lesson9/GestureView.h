//
//  GestureView.h
//  Lesson9
//
//  Created by iOS-School-2 on 24.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol GestureViewDelegate;

@interface GestureView : UIView

@property (nonatomic, weak) id<GestureViewDelegate> delegate;
- (void)switchColor;
- (void)switchToColorWithRed:(CGFloat)red
                       green:(CGFloat)green
                        blue:(CGFloat)blue;


@end
