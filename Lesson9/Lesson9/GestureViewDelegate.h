//
//  GestureViewDelegate.h
//  Lesson9
//
//  Created by iOS-School-2 on 24.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GestureView;

@protocol GestureViewDelegate <NSObject>

- (void)userDidTapOnGestureView:(GestureView *)sender;

@optional
- (void)userDidSwipeUpGestureView:(GestureView *)sender;
- (void)userDidSwipeDownGestureView:(GestureView *)sender;

@end
