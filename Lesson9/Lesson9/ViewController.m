//
//  ViewController.m
//  Lesson9
//
//  Created by iOS-School-2 on 24.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "ViewController.h"
#import "GestureView.h"
#import "GestureViewDelegate.h"
#import "ColorRandomizer.h"

@interface ViewController () <GestureViewDelegate>

@property (weak, nonatomic) IBOutlet GestureView *gestureView;
@property (strong, nonatomic) ColorRandomizer *colorRandomizer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.gestureView.delegate = self;
}

- (ColorRandomizer *)colorRandomizer {
    if (!_colorRandomizer) {
        _colorRandomizer = [ColorRandomizer new];
    }
    
    return _colorRandomizer;
}

#pragma mark - GestureViewDelegate implementation

- (void)userDidTapOnGestureView:(GestureView *)sender {
//    [self.gestureView switchColor];
    
    CGFloat red = [self.colorRandomizer colorComponent] / 255.0f;
    CGFloat green = [self.colorRandomizer colorComponent] / 255.0f;
    CGFloat blue = [self.colorRandomizer colorComponent] / 255.0f;
    [sender switchToColorWithRed:red green:green blue:blue];
}

-(void)userDidSwipeUpGestureView:(GestureView *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        sender.frame = CGRectMake(sender.frame.origin.x,
                                  sender.frame.origin.y,
                                  sender.frame.size.width * 1.1,
                                  sender.frame.size.height * 1.1);
        sender.center = self.view.center;
    }];
}

-(void)userDidSwipeDownGestureView:(GestureView *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        sender.frame = CGRectMake(sender.frame.origin.x,
                                  sender.frame.origin.y,
                                  sender.frame.size.width * 0.9,
                                  sender.frame.size.height * 0.9);
        sender.center = self.view.center;
    }];
}

@end
