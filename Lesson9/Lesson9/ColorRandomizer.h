//
//  ColorRandomizer.h
//  Lesson9
//
//  Created by iOS-School-2 on 24.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorRandomizer : NSObject

- (NSUInteger)colorComponent;

@end
