//
//  ColorRandomizer.m
//  Lesson9
//
//  Created by iOS-School-2 on 24.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "ColorRandomizer.h"

@implementation ColorRandomizer

-(NSUInteger)colorComponent {
    NSInteger result = arc4random() % 255;
    return ABS(result);
}

@end
