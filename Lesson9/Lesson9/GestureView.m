//
//  GestureView.m
//  Lesson9
//
//  Created by iOS-School-2 on 24.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "GestureView.h"
#import "GestureViewDelegate.h"

@implementation GestureView

#pragma mark - Initialization.

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return self;
    
}

- (void)configure {
    UITapGestureRecognizer *tapGesture;
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSelf)];
    [self addGestureRecognizer:tapGesture];
    
    
    UISwipeGestureRecognizer *swipeGestureUp;
    swipeGestureUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeSelfUp)];
    swipeGestureUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self addGestureRecognizer:swipeGestureUp];
    
    
    UISwipeGestureRecognizer *swipeGestureDown;
    swipeGestureDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeSelfDown)];
    swipeGestureDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipeGestureDown];
    
    UIPanGestureRecognizer *panGesture;
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panOnView:)];
    [self addGestureRecognizer:panGesture];
    
}

#pragma mark - Actions.
- (void)tapSelf {
    if (self.delegate) {
        [self.delegate userDidTapOnGestureView:self];
    }
}
                                                                                         
- (void)swipeSelfUp {
    BOOL isRespondsToSelector = [self.delegate respondsToSelector:@selector(userDidSwipeUpGestureView:)];
    if (isRespondsToSelector) {
        [self.delegate userDidSwipeUpGestureView:self];
    }
}

- (void)swipeSelfDown {
    BOOL isRespondsToSelector = [self.delegate respondsToSelector:@selector(userDidSwipeDownGestureView:)];
    if (isRespondsToSelector) {
        [self.delegate userDidSwipeDownGestureView:self];
    }
}

- (void)panOnView:(UIPanGestureRecognizer *)panGesture {
    CGPoint translation = [panGesture translationInView:self.superview];
    [panGesture setTranslation:CGPointZero inView:self];
    
    self.center = CGPointMake(self.center.x + translation.x, self.center.y + translation.y);


    
//    NSLog(@"\n\nVELOCITY: X:%f Y:%f", [panGesture velocityInView:self].x, [panGesture velocityInView:self].y);
//    NSLog(@"\n\nLOCATION: X:%f Y:%f", [panGesture locationInView:self].x, [panGesture locationInView:self].y);
    
}


-(void)switchColor {
    self.backgroundColor = [UIColor blueColor];
}


-(void)switchToColorWithRed:(CGFloat)red
                      green:(CGFloat)green
                       blue:(CGFloat)blue {
    self.backgroundColor = [UIColor colorWithRed:red
                                           green:green
                                            blue:blue
                                           alpha:1];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
